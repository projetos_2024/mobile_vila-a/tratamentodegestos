import React from 'react';
import { Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native'; 
import { createStackNavigator } from '@react-navigation/stack';
import CountMoviment from './src/countMoviment';
import Carrossel from './src/countImagem';

const Stack = createStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Carrossel">
   
        <Stack.Screen name="Carrossel" component={Carrossel} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
