import React , {useRef, useState,useEffect} from "react";
import { View,Dimensions, PanResponder, Button,Text} from "react-native";

const CountMoviment = () =>{
    const [count,setCount] = useState(0);
    const screenHeight = Dimensions.get("window").height
    const gestureThreshold = screenHeight * 0.25;
    const panResponder = useRef(

        PanResponder.create({
            onStartShouldSetPanResponder:()=>true,
            onPanResponderMove:(event,gestureState)=>{},
            onPanResponderRelease:(event,gestureState)=>{
                if(gestureState.dy < -gestureThreshold){
                    setCount((prevCount) =>prevCount+1)
                }
                
            }
        })

    ).current;
    return(
        <View {...panResponder.panHandlers} style={{flex:1,alignItems:'center', justifyContent:'center'}}>
            <Text>Valor do Contador:{count}</Text>
        </View>
    );
};
export default CountMoviment;




'https://i.pinimg.com/236x/d8/61/ca/d861ca7a13ee073d4b70fced22ebc097.jpg',
'https://i.ytimg.com/vi/lA0zHAeyJaI/maxresdefault.jpg',
'https://pbs.twimg.com/media/F17sbF1WcAQQTPo.jpg'

